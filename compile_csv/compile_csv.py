#!/usr/bin/env python3

import sys, glob, pandas

# get command line arguments
if len(sys.argv) < 2:
    print('usage:', sys.argv[0], ' input_dir output_file')
    sys.exit(-1)
input_dir = sys.argv[1]
output_file = sys.argv[2]

# read csv files and append dataframe
df = pandas.DataFrame()
for csv_file in glob.glob(input_dir + '*.csv'):
    try:
        file_df = pandas.read_csv(csv_file, sep=';')
        df = df.append(file_df, ignore_index=True)
    except:
        print('warning:', csv_file, 'is empty')

# export dataframe to csv
df.to_csv(output_file, index=False, sep=';')

