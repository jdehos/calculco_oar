# oar scripts

## basic

```
cd oar_basic
oarsub -S ./oar_basic.sh
```

## array

```
cd oar_array
./gen_params.sh
oarsub -S ./oar_array.sh
```

## dispatch

```
cd oar_dispatch
./gen_params.sh
./dispatch.sh
```

## compile csv

```
cd compile_csv
./compile_csv.py ../ouput/ data.csv
```

