#!/bin/sh

nb_cores=3

nb_lines=`wc -l params.txt | cut -d " " -f 1`
step=`echo "1 + $nb_lines / $nb_cores" | bc`

for i in `seq 1 $nb_cores` ; do

    param_file="tmp_params_${i}.txt"
    i0=`echo "1 + ($i - 1) * $step" | bc`
    i1=`echo "$i0 + $step - 1" | bc`
    echo "computing range $i0 - $i1"
    sed -n ${i0},${i1}p params.txt > ${param_file}

    script_file="tmp_oar_${i}.sh"
    rm -f ${script_file}
    echo "#!/bin/sh" >> ${script_file}
    echo "#OAR -l /core=1,walltime=00:05:00" >> ${script_file}
    echo "#OAR -p host=\"orval02\"" >> ${script_file}
    echo "#OAR -t besteffort" >> ${script_file}
    echo "#OAR -t idempotent" >> ${script_file}
    echo "./my_sub.sh ${param_file}" >> ${script_file}
    chmod +x ${script_file}

    oarsub -S ./${script_file}

done

