#!/bin/sh

if [ $# -ne 1 ] ; then
    echo "usage: $0 <param file>"
    exit
fi
file=$1

while read line ; do
    if [ ! -z "${line}" ] ; then  # ignore empty line
        ../bin/run_mul.sh ${line}
    fi
done < ${file}

