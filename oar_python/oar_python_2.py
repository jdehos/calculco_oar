#!/usr/bin/env python3

#OAR -l /core=6,walltime=00:05:00
CORE=6

import joblib

def print_hello(x):
    print("Hello", x)

if __name__ == "__main__":
    joblib.Parallel(n_jobs=CORE)(joblib.delayed(print_hello)(i) for i in range(12))

