#!/usr/bin/env python3

#OAR -l /core=1,walltime=00:05:00

def print_hello(x):
    print("Hello", x)

if __name__ == "__main__":
    for i in range(12):
        print_hello(i)

