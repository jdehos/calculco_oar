#!/bin/sh

OUTDIR="../output"

params=`echo $@ | tr " " "_"`
output_path="${OUTDIR}/mul_`hostname`_`date +%Y-%m-%d`_`date +%H-%M-%S`_${params}.csv"

mkdir -p ${OUTDIR}
echo $@

../bin/mul.sh $@ 1> ${output_path}

