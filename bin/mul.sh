#!/bin/sh

if [ $# -ne 2 ] ; then
    echo "usage: $0 <x> <y>"
    exit
fi

x=$1
y=$2
r=`echo "$x * $y" | bc`

echo "x;y;r"
echo "$x;$y;$r"

