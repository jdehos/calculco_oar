#include "mytest.hpp"

#include <iostream>
#include <stdlib.h>

int main(int argc, char ** argv) {
    if (argc != 2) {
        std::cout << "usage: " << argv[0] << " <x>\n";
        exit(-1);
    }
    std::cout << doubler(atoi(argv[1])) << std::endl;
    return 0;
}

