#!/usr/bin/env python3

def doubler(n):
    return 2*n

if __name__ == "__main__":
    print(doubler(21))

