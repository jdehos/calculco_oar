
```
# build and install project
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=~/opt/mytest ..
make -j install
cd ..

# use module
module purge
module use ~/opt/mytest/modules
module load mytest
mytest_main.out 21
mytest.py
```

